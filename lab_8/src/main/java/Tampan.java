import pekerja.*; 
import java.util.ArrayList;

public class Tampan{
	ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
	
	public Karyawan cari(String nama){
		for(Karyawan namaPekerja : karyawan){
			if(namaPekerja.getNama().equals(nama)){
				return namaPekerja;
			}
		}
		return null;
	}
	
	public String inputBatasGaji(int batasGajiParse){
		for(Karyawan namaPekerja : karyawan){
			if(namaPekerja != null){
				namaPekerja.setBatasGaji(batasGajiParse);
			}
		}
		String printBatasGaji = String.format("Batas gaji untuk karyawan adalah %d", batasGajiParse);
		return printBatasGaji;
	}
	
	public String tambah(String tipe, String namaPekerja, int gajiAwal ){
		if(cari(namaPekerja) == null){
			if(tipe.equalsIgnoreCase("manager")){
				Karyawan worker = new Manager(namaPekerja, gajiAwal, "Manager");
				karyawan.add(worker);
			}
			else if(tipe.equalsIgnoreCase("staff")){
				Karyawan worker = new Staff(namaPekerja, gajiAwal, "Staff");
				karyawan.add(worker);
			}
			else{
				Karyawan worker = new Intern(namaPekerja, gajiAwal, "Intern");
				karyawan.add(worker);
			}
			return namaPekerja + " mulai bekerja sebagai " + tipe + " di PT.TAMPAN";
		}
		return "Karyawan dengan nama " + namaPekerja + " telah terdaftar";
	}
	
	public String status(String pekerja){
		Karyawan namaPekerja = cari(pekerja);
		if(namaPekerja != null){
			String printStatus = String.format("%s %d", pekerja, namaPekerja.getGaji());
			return printStatus;
		}
		return "Karyawan bernama " + pekerja + " belum pernah dimasukan sebelumnya.";
		
	}

	public String tambahKaryawan(String atasan, String bawahan){
		Karyawan namaPekerjaAtasan = cari(atasan);
		Karyawan namaPekerjaBawahan = cari(bawahan);
		if(namaPekerjaAtasan == null || namaPekerjaBawahan == null){
			return "Nama tidak berhasil ditemukan";
		}
		else if(namaPekerjaAtasan.getTipe().equalsIgnoreCase("manager")){
			Karyawan cariBawahanManager = namaPekerjaAtasan.cariBawahan(namaPekerjaBawahan);
			String tipeBawahan = namaPekerjaBawahan.getTipe();
			if(cariBawahanManager != null){
				return "Anda tidak layak memiliki bawahan";
			}
			else if(tipeBawahan.equals("manager")){
				return "Anda tidak layak memiliki bawahan";
			}
			else{
				namaPekerjaAtasan.rekrut(namaPekerjaBawahan);
				return "Karyawan " + bawahan + " berhasil ditambahkan menjadi bawahan " + atasan;
			}
		}
		else if(namaPekerjaAtasan.getTipe().equalsIgnoreCase("staff")){
			Karyawan cariBawahanStaff = namaPekerjaAtasan.cariBawahan(namaPekerjaBawahan);
			String tipeBawahan = namaPekerjaBawahan.getTipe();
			if(cariBawahanStaff != null){
				return "Anda tidak layak memiliki bawahan";
			}
			else if(tipeBawahan.equalsIgnoreCase("manager") || tipeBawahan.equalsIgnoreCase("staff")){
				return "Anda tidak layak memiliki bawahan";
			}
			else{
				namaPekerjaAtasan.rekrut(namaPekerjaBawahan);
				return "Karyawan " + bawahan + " berhasil ditambahkan menjadi bawahan " + atasan;
			}
		}
		else{
			return "Anda tidak layak memiliki bawahan";
		}
	}
	
	public String gajian(){
		for(Karyawan namaPekerja : karyawan){
			if(namaPekerja != null){
				namaPekerja.gajiPekerja();
			}
		}
		return "Semua karyawan telah diberikan gaji";
	}
}