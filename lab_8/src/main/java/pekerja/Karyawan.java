package pekerja;

import java.util.ArrayList;

public class Karyawan{
	protected String nama;
	protected String tipe;
	protected int gaji;
	protected int konterGaji = 0;
	protected ArrayList<Karyawan> listBawahan = new ArrayList<Karyawan>();
	protected int batasGajiStaff;;
	
	public Karyawan(String nama, int gaji, String tipe){
		this.nama = nama;
		this.tipe = tipe;
		this.gaji = gaji;
	}
	
	public String getNama(){
		return this.nama;
	}
	
	public int getGaji(){
		return this.gaji;
	}
	
	
	public String getTipe(){
		return this.tipe;
	}
	
	public void setTipe(String tipe){
		this.tipe = tipe;
	}
	
	public int getKonterGaji(){
		return this.konterGaji;
	}
	
	public void setBatasGaji(int batasGaji){
		this.batasGajiStaff = batasGaji;
	}
	
	public void rekrut(Karyawan namaBawahan){
		if(this.listBawahan.size()<=10){
			this.listBawahan.add(namaBawahan);
		}
		else if(cariBawahan(namaBawahan) == null){
			this.listBawahan.add(namaBawahan);
		}
		else{
			System.out.println("Anda tidak layak memiliki bawahan");
		}
	}
	
	public Karyawan cariBawahan(Karyawan bawahan){
		for(Karyawan namaBawahan : listBawahan){
			if(namaBawahan.nama.equals(bawahan)){
				return namaBawahan;
			}
		}
		return null;
	}
	
	 public void gajiPekerja(){
        this.konterGaji += 1;
        if (this.konterGaji % 6 == 0){
            this.gaji += this.gaji / 10;
        }
        if (this.getTipe().equalsIgnoreCase("staff")){
            if (this.gaji > this.batasGajiStaff){
                this.setTipe("Manager");
            }
        }
    }
}