package pekerja;

public class Intern extends Karyawan{
	public Intern(String nama, int gaji){
		super(nama, gaji, "Intern");
	}
	
	public Intern(String nama, int gaji, String tipe){
		super(nama, gaji, tipe);
	}
}