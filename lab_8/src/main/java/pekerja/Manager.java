package pekerja;

public class Manager extends Karyawan{
	public Manager(String nama, int gaji){
		super(nama, gaji, "Manager");
	}
	
	public Manager(String nama, int gaji, String tipe){
		super(nama, gaji, tipe);
	}
}