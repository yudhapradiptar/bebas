package pekerja;

public class Staff extends Karyawan{
	public Staff(String nama, int gaji){
		super(nama, gaji, "Staff");
	}
	
	public Staff(String nama, int gaji, String tipe){
		super(nama, gaji, tipe);
	}
}