import java.util.Scanner;

class Lab8 {
	private static Scanner input = new Scanner(System.in);
	
    public static void main(String[] args) {
		Tampan office = new Tampan();
		while(true){
			String inputUser = input.nextLine();
			String[] inputSplit = inputUser.split(" ");
			if(inputSplit[0].toUpperCase().equals("TAMBAH_KARYAWAN")){
				int inputSplitGaji = Integer.parseInt(inputSplit[3]);
				System.out.println(office.tambah(inputSplit[2], inputSplit[1], inputSplitGaji));
			}
			else if(inputSplit[0].toUpperCase().equals("STATUS")){
				System.out.println(office.status(inputSplit[1]));
			}
			else if(inputSplit[0].toUpperCase().equals("GAJIAN")){
				System.out.println(office.gajian());
			}
			else if(inputSplit[0].toUpperCase().equals("TAMBAH_BAWAHAN")){
				System.out.println(office.tambahKaryawan(inputSplit[1], inputSplit[2]));
			}
			else if(inputSplit[0].toUpperCase().equals("SELESAI")){
				break;
			}
			else if(inputSplit[0].equals("5000")||inputSplit[0].equals("18000")){
				int batasGajiParse = Integer.parseInt(inputSplit[0]);
				System.out.println(office.inputBatasGaji(batasGajiParse));
			}
			else{
				System.out.println("Salah Input");
			}
		}
    }
}