/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan//s
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

import java.util.Scanner;
 public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}
	public boolean isBingo(){
		return isBingo;
	}
	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public void checkBingo() {
		if (numbers[0][0].isChecked() && numbers[1][1].isChecked() && numbers[2][2].isChecked() && numbers[3][3].isChecked() && numbers[4][4].isChecked() == true){
			this.setBingo(true);
		}
		boolean flag = true;
		for(int i = 0;i<5;i++){
			for(int j=0;j<5;j++){
				if(i==j){
					if(numbers[i][j].isChecked() == false){
						flag = false;
					}
					
					
				}
			}
		}
		else if((numbers[0][4].isChecked() && numbers[1][3].isChecked() && numbers[2][2].isChecked() && numbers[3][1].isChecked() && numbers[4][0].isChecked()) == true ){
			this.setBingo(true);
		}
		else{
			for (int a=0;a<5;a++){
				if((numbers[a][0].isChecked() && numbers[a][1].isChecked() && numbers[a][2].isChecked() && numbers[a][3].isChecked() && numbers[a][4].isChecked()) == true ){
					this.setBingo(true);
					break;
				}
				else if((numbers[0][a].isChecked() && numbers[1][a].isChecked() && numbers[2][a].isChecked() && numbers[3][a].isChecked() && numbers[4][a].isChecked()) == true ){
					this.setBingo(true);
					break;
				}
			}
		}
		//return isBingo;
	}

	public String markNum(int num){
		String markBalik = "";
		if(numberStates[num] != null){
			if (numberStates[num].isChecked() == false){
				numberStates[num].setChecked(true);
				markBalik = (String.valueOf(num) + " tersilang");
				this.checkBingo();
			}else {
				markBalik = (String.valueOf(num) + " sebelumnya sudah tersilang");;
			}
		}else{
			markBalik = ("Kartu tidak memiliki angka " + String.valueOf(num));
		}
		return markBalik;
	}

	
	public String info(){
		String infoBalik = "";
		for (int r=0;r<5;r++){
			infoBalik += ("|");
			for (int c=0;c<5;c++){
				if(numbers[r][c].isChecked()) {
					infoBalik += (" X  |");
				}else{
					infoBalik += (" " + String.valueOf(numbers[r][c].getValue()) + " |");
				}
			}
			if (r != 4){
			infoBalik += ("\n");
			}
		}
		return infoBalik;
		
	}
	
	public void restart(){
		for (int a = 0; a < 5; a++) {
			for (int b = 0; b < 5; b++) {
				numbers[a][b].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
	
	public static void main(String[] args){
		Number[][] numbers = new Number[5][5];
		Number[] states = new Number[100];
		Scanner input = new Scanner(System.in);
		for (int i=0;i<5;i++){
			String rowNumber = input.nextLine();
			String[] numberRowSplit = rowNumber.split(" ");
			for (int j=0;j<5;j++){
				int numberParse = Integer.parseInt(numberRowSplit[j]);
				numbers[i][j] = new Number(numberParse,i,j);
				states[numbers[i][j].getValue()] = numbers[i][j];
			}
		}
		BingoCard card = new BingoCard(numbers, states);
		while(true){
			String inputUser = input.nextLine();
			String[] inputPerintah = inputUser.split(" "); 
			
			if (inputPerintah[0].toUpperCase().equals("MARK")){
				card.markNum(Integer.parseInt(inputPerintah[1]));
				System.out.println(card.markNum(Integer.parseInt(inputPerintah[1])));
				if(card.isBingo()){
					System.out.println(card.info());
					System.out.println("BINGO!");
					break;
				}
			}
			else if(inputPerintah[0].toUpperCase().equals("INFO")){
				System.out.println(card.info());
			}
			else if(inputPerintah[0].toUpperCase().equals("RESTART")){
				card.restart();
			}
			else {
				System.out.println("Incorrect command");
			}
		}
	}
	

}
