package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.util.XoxoMessage;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;
	
	private static int countEncryption = 1;

    private static int countDecryption = 1;

    private final static String pathOut = "C:\\Users\\Yudha Pradipta R\\Documents\\DDP 2\\TutorialDDP2\\lab_10\\out\\";

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoDecryption decryption = new XoxoDecryption(gui.getKeyText());
                String message = null;
                if (gui.getSeedText().equals("DEFAULT_SEED")){
                    message = decryption.decrypt(gui.getMessageText(), 18);
                }
                else {
                    message = decryption.decrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText()));
                }

                String toPrint = "";
                toPrint += (message + " is decrypted from code " + gui.getMessageText() + " with key " + gui.getKeyText() + " and seed " + gui.getSeedText());

                FileWriter fw = null;
                File outputDecrypt = new File(pathOut + "outputDecrypt_" + countDecryption + ".txt");

                try {
                    fw = new FileWriter(outputDecrypt);
                    fw.write(toPrint);
                    fw.flush();
                    fw.close();
                }catch (IOException a){
                    a.printStackTrace();
                }

                gui.appendLog("Result of decryption has been added to outputDecryption_" + countDecryption + ".txt!");

                countDecryption += 1;
            }
        });
		
		gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoEncryption encryption = new XoxoEncryption(gui.getKeyText());
                XoxoMessage message = null;
                if (gui.getSeedText().equals("DEFAULT_SEED")){
                    message = encryption.encrypt(gui.getMessageText());
                }
                else {
                    message = encryption.encrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText()));
                }
                String toPrint = "";
                toPrint += (message + " is encrypted from code " + gui.getMessageText() + " with key " + gui.getKeyText() + " and seed " + gui.getSeedText());

                FileWriter fw = null;
                File outputEncrypt = new File(pathOut + "outputEncrypt_" + countEncryption + ".enc");

                try {
                    outputEncrypt.createNewFile();
                    fw = new FileWriter(outputEncrypt);
                    fw.write(toPrint);
                    fw.flush();
                    fw.close();
                }catch (IOException a){
                    a.printStackTrace();
                }

                gui.appendLog("Result of encryption has been added to outputEncryption_" + countEncryption + ".enc!");

                countEncryption += 1;
            }
        });
		
    }

    //TODO: Create any methods that you want
}