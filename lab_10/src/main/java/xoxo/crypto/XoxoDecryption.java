package xoxo.crypto;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed){
        //TODO: Implement decryption algorithm

		final int encryptedLength = encryptedMessage.length();
		String decryptedMessage = "";
		for(int i = 0;i<encryptedLength;i++){
			int modHugKey = this.hugKeyString.charAt((i % this.hugKeyString.length()));
			int xorSeed = modHugKey ^ seed;
			int minA = xorSeed - 'a';
			int combine = encryptedMessage.charAt(i) ^ minA;
			decryptedMessage += (char) combine;
		}
		return decryptedMessage;

    }
}