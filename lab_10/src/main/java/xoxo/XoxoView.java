package xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField = new JTextField();
	
    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField = new JTextField(30);

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField = new JTextField(30);

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField = new JTextArea(20, 20); 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton = new JButton("Encrypt");

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton = new JButton("Decrypt");
	
	private JScrollPane scrollLog = new JScrollPane(logField);
	


    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
		JFrame jf = new JFrame("Xoxo");
		Container c = jf.getContentPane();
		c.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
		
		
		c.add(new JLabel("Seed: "),gbc);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.5;
        gbc.ipadx = 20;
        gbc.ipady = 20;
        gbc.gridx = 1;
        gbc.gridy = 0;
        c.add(seedField, gbc);


        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.5;
        gbc.ipadx = 20;
        gbc.ipady = 20;
        gbc.gridx = 0;
        gbc.gridy = 1;
        c.add(new JLabel("Key: "),gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.5;
        gbc.ipadx = 20;
        gbc.ipady = 20;
        gbc.gridx = 1;
        gbc.gridy = 1;
        c.add(keyField, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.5;
        gbc.ipadx = 20;
        gbc.ipady = 20;
        gbc.gridx = 0;
        gbc.gridy = 2;
        c.add(new JLabel("Message: "), gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.ipadx = 20;
        gbc.ipady = 20;
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        gbc.gridy = 2;
        c.add(messageField, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.5;
        gbc.ipadx = 30;
        gbc.ipady = 30;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        gbc.gridy = 3;
        c.add(decryptButton, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.5;
        gbc.ipadx = 30;
        gbc.ipady = 30;
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 3;
        c.add(encryptButton, gbc);


        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.ipady = 30;
        gbc.weightx = 0.0;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        gbc.gridy = 4;
        logField.setEditable(false);
        c.add(scrollLog, gbc);

        jf.setLocation(600,250);
        jf.pack();
        jf.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}