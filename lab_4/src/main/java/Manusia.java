public class Manusia {
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	
	public Manusia(String nama, int umur) {
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
	}
	public Manusia(String nama, int umur, int uang) {
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public void setUmur(int umur) {
		this.umur = umur;
	}
	
	public void setUang(int uang) {
		this.uang = uang;
	}
	
	public void setKebahagiaan(float kebahagiaan) {
		this.kebahagiaan = kebahagiaan;
	}
	
	public String getNama() {
		return nama;
	}
	
	public int getUmur() {
		return umur;
	}
	
	public int getUang() {
		return uang;
	}
	
	public float getKebahagiaan() {
		return kebahagiaan;
	}
	
	public void beriUang(Manusia penerima) {
		int ascii = 0;
		for (int a = 0; a < penerima.nama.length(); a++){
			ascii += penerima.nama.charAt(a);
			int jumlah_uang = ascii * 100;
		}
		int jumlah_uang = ascii * 100;
		if (this.uang < jumlah_uang) {
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
		else {
			this.uang -= jumlah_uang;
			penerima.uang += jumlah_uang;
			this.kebahagiaan += (float) jumlah_uang/6000;
			penerima.kebahagiaan += (float) jumlah_uang/6000;
			if (this.kebahagiaan < 0) {
				this.kebahagiaan = 0;
			}
			else if (this.kebahagiaan > 100) {
				this.kebahagiaan = 100;
			}
			if (penerima.kebahagiaan < 0) {
				penerima.kebahagiaan = 0;
			}
			else if (penerima.kebahagiaan > 100) {
				penerima.kebahagiaan = 100;
			}
			System.out.println(this.nama +" memberi uang sebanyak "+ jumlah_uang + " kepada " + penerima.nama +", mereka berdua senang :D");
		}
			
	}
	
	public void beriUang(Manusia penerima, int jumlah_uang) {
		if (this.uang < jumlah_uang) {
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
		else {
			this.uang -= jumlah_uang;
			penerima.uang += jumlah_uang;
			this.kebahagiaan += (float) jumlah_uang/6000;
			penerima.kebahagiaan += (float) jumlah_uang/6000;
			if (this.kebahagiaan < 0) {
				this.kebahagiaan = 0;
			}
			else if (this.kebahagiaan > 100) {
				this.kebahagiaan = 100;
			}
			if (penerima.kebahagiaan < 0) {
				penerima.kebahagiaan = 0;
			}
			else if (penerima.kebahagiaan > 100) {
				penerima.kebahagiaan = 100;
			}
			System.out.println(this.nama +" memberi uang sebanyak "+ jumlah_uang + " kepada " + penerima.nama +", mereka berdua senang :D");
		}
	}
		
	public void bekerja(int durasi, int bebanKerja) {
		int bebanKerjaTotal = durasi * bebanKerja;
		if (this.umur < 18) {
			System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
		}
		else if (bebanKerjaTotal <= this.kebahagiaan) {
			this.kebahagiaan -= bebanKerjaTotal;
			int pendapatan = bebanKerjaTotal * 10000;
			System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
			this.uang += pendapatan;
		}
		else if (bebanKerjaTotal > this.kebahagiaan) {
			int durasiBaru = (int) this.kebahagiaan/bebanKerja;
			bebanKerjaTotal = durasiBaru * bebanKerja;
			int pendapatan = bebanKerjaTotal * 10000;
			this.kebahagiaan -= bebanKerjaTotal;
			this.uang += pendapatan;
			System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
		}
	}
	
	public void rekreasi(String namaTempat) {
		int biaya = namaTempat.length() * 10000;
		if (biaya > this.uang) {
			System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat);
		}
		else if (biaya <= this.uang) {
			this.kebahagiaan += namaTempat.length();
			if (this.kebahagiaan > 100) {
				this.kebahagiaan = 100;
			}
			this.uang -= biaya;
			System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang");
		}
	}
	
	public void sakit(String namaPenyakit) {
		this.kebahagiaan -= (float)namaPenyakit.length();
		if (this.kebahagiaan < 0) {
			this.kebahagiaan = 0;
		}
		System.out.println(this.nama + " terkena penyakit " + namaPenyakit);
	}
	public String toString() {
		return ("Nama\t\t: " + nama + "\nUmur\t\t: " + umur + "\nUang\t\t: " + uang + "\nKebahagiaan\t: " + kebahagiaan); //komen
		
	}
}

			
			
			
			
		