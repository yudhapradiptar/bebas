package lab9.event;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    private LocalDateTime localTimeBegin;
    private LocalDateTime localTimeEnd;
    private BigInteger costPerHour;
	
	public Event(String name, LocalDateTime timeBegin, LocalDateTime timeEnd, String costPerHour){
		this.name=name;
		this.localTimeBegin=timeBegin;
		this.localTimeEnd=timeEnd;
		this.costPerHour= new BigInteger(costPerHour);
	}
    
    // TODO: Make instance variables for representing beginning and end time of event
    
    // TODO: Make instance variable for cost per hour
    
    // TODO: Create constructor for Event class
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }
	
	
	public String toString(){
		String printedtext = "";
        printedtext += this.getName() + "\n";
        DateTimeFormatter formatterdate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter formatterhour = DateTimeFormatter.ofPattern("HH:mm:ss");
        printedtext += String.format("Waktu mulai: %s, %s%n", this.localTimeBegin.format(formatterdate), this.localTimeBegin.format(formatterhour) );
        printedtext += String.format("Waktu selesai: %s, %s%n", this.localTimeEnd.format(formatterdate), this.localTimeEnd.format(formatterhour));
        printedtext += String.format("Biaya kehadiran: %s%n", this.getCost());
        return printedtext;
	}
	
	public boolean checkTime(Event otherEvent){
        boolean isOverlap = false;
        if (otherEvent.localTimeBegin.isBefore(this.localTimeEnd) && otherEvent.localTimeEnd.isAfter(this.localTimeBegin)) {
            isOverlap = true;
        }
        return isOverlap;
	}
	
    public String getCost(){
        return String.valueOf(this.costPerHour);
    }
	public BigInteger getCostPerHour(){
		return this.costPerHour;
	}
	
	public int compareTo(Event other) {
		if (this.localTimeBegin.equals(other.localTimeBegin)){
			if (this.localTimeEnd.equals(other.localTimeEnd)){
				return this.name.compareTo(other.name);
			}
			return this.localTimeEnd.compareTo(other.localTimeEnd);
		}
		else{
			return this.localTimeBegin.compareTo(other.localTimeBegin);
		}
	}
	
	
    
    // TODO: Implement toString()
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
