package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
    public Event findEvent(String eventName){
		for(Event namaEvent : events){
			if(namaEvent.getName().equalsIgnoreCase(eventName)){
				return namaEvent;
			}
		}
		return null;
	}
	
	public User findUser(String userName){
		for(User namaUser : users){
			if(namaUser.getName().equalsIgnoreCase(userName)){
				return namaUser;
			}
		}
		return null;
	}

    public LocalDateTime parseLocalDateTime(String time){
        String pattern  = "yyyy-MM-dd_HH:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime localtime = LocalDateTime.parse(time, formatter);
        return localtime;
    }
	
	public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        String text = "";
        if (findEvent(name) == null){
            LocalDateTime x = parseLocalDateTime(startTimeStr);
            LocalDateTime y = parseLocalDateTime(endTimeStr);
            boolean z = x.isBefore(y);
            if (parseLocalDateTime(startTimeStr).isBefore(parseLocalDateTime(endTimeStr))){
                Event newEvent = new Event(name, parseLocalDateTime(startTimeStr), parseLocalDateTime(endTimeStr), costPerHourStr);
                events.add(newEvent);
                text += String.format("Event %s berhasil ditambahkan!%n", name);
            }
            else {
                text += String.format("Waktu yang diinputkan tidak valid!%n");
            }
        }else{
            text += String.format("Event %s sudah ada!%n", name);
        }
        return text;
    }
    
    public String addUser(String name)
    {
		if(findUser(name) == null){
			User pengguna = new User(name);
			users.add(pengguna);
			return "user " + name + " sudah ditambahkan"; 
		}
		return "User " + name + " sudah ada!";
    }
    
    public String registerToEvent(String userName, String eventName)
    {
        User userRegister = findUser(userName);
		Event eventRegister = findEvent(eventName);
		if(userRegister == null && eventRegister == null){
			return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
		}
		
		else if(userRegister == null){
			return "Tidak ada pengguna dengan nama " + userName + "!";
		}
		
		else if(eventRegister == null){
			return "Tidak ada acara dengan nama " + eventName + "!";
		}
		else{
			userRegister.addEvent(eventRegister);
			return "userName berencana menghadiri " + eventName + "!";
		}
		
    }
	
	public Event getEvent(String eventname){
        return findEvent(eventname);
    }

    public User getUser(String username){
        return findUser(username);
    }
	
	
}