import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Yudha Pradipta Ramadan, NPM 1706043424, Kelas C, GitLab Account: yudhapradiptar
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjang = input.nextLine();
		int inputPanjang = Integer.parseInt(panjang);
		if (inputPanjang<= 0 || inputPanjang>= 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		String lebar = input.nextLine();
		int inputLebar = Integer.parseInt(lebar);
		if (inputLebar<= 0 || inputLebar>= 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggi = input.nextLine();
		int inputTinggi = Integer.parseInt(tinggi);
		if (inputTinggi<= 0 || inputTinggi>= 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		String berat = input.nextLine();
		float inputBerat = Float.parseFloat(berat);
		if (inputBerat<= 0 || inputBerat>= 150){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		String makanan = input.nextLine();
		int inputMakanan = Integer.parseInt(makanan);
		if (inputMakanan<= 0 || inputMakanan>= 20){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahCetakan = input.nextLine();
		int inputJumlahCetakan = Integer.parseInt(jumlahCetakan);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		
		int rasio = (int) (((inputBerat)*1000000)/((inputPanjang)*(inputLebar)*(inputTinggi)));

		for (int nomor = 1; nomor<(inputJumlahCetakan + 1); nomor++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + nomor + " dari " + inputJumlahCetakan + " untuk: ");
			String penerima = (input.nextLine());
			String penerimaUpper = (penerima.toUpperCase());
			// Lakukan baca input lalu langsung jadikan uppercase

			System.out.println("DATA SIAP DICETAK UNTUK " + penerimaUpper);
			System.out.println(nama + " - " + alamat);
			System.out.println("Lahir pada tanggal " + tanggalLahir);
			System.out.println("Rasio Berat Per Volume = " + "rasio" + " kg/m^3");
			// TODO Periksa ada catatan atau tidaka
			if (catatan.isEmpty()){
				System.out.println("Catatan : Tidak ada catatan tambahan");
			}
			else {	
				// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
				System.out.println("Catatan : " + catatan);
			}
		}
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int ascii = 0;
		for (int a = 0; a < nama.length(); a++){
			ascii += nama.charAt(a);
		}
		int nomorPengenalKeluarga = (((inputPanjang * inputLebar * inputTinggi) + ascii) % 10000);
		String stringPengenal = Integer.toString(nomorPengenalKeluarga);


		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + stringPengenal;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000 * 365 * inputMakanan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		
		String[] parts = tanggalLahir.split("-");;
		String tahunLahirParts = parts[2];
		int tahunLahir = Integer.parseInt(tahunLahirParts);// lihat hint jika bingung
		int umur = (2018) - (tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		System.out.println("REKOMENDASI APARTEMEN");
		System.out.println("--------------------");
		System.out.println("MENGETAHUI: Identitas Keluarga: " + nama + " - " + nomorKeluarga);
		System.out.println("MENIMBANG: Anggaran makanan tahunan: Rp " + anggaran);
		System.out.println("           Umur kepala keluarga: " + umur + " tahun");
		
		if (umur>=0 && umur<=18){
			System.out.print("MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n PPMT, kabupaten Rotunda");
		}	
		else if ((umur>=19 && umur<= 1018) && (anggaran>=0 && anggaran<= 100000000)){
			System.out.print("MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n Teksas, kabupaten Sastra");
		}
		else if ((umur>=19 && umur<=1018) && (anggaran>100000000)){
			System.out.print("MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n Mares, kabupaten Margonda");
		}
		input.close();
	}
}
