package movie;
public class Movie{
	private String judul;
	private String genre;
	private int durasiMenit;
	private String rating;
	private String asalFilm;

	public Movie(String judul, String rating, int durasiMenit, String genre, String asalFilm){
		this.judul = judul;
		this.genre = genre;
		this.durasiMenit = durasiMenit;
		this.rating = rating;
		this.asalFilm = asalFilm;
	}
		
	public String getJudul(){
		return judul;
	}
	public String getGenre(){
		return genre;
	}
	public int getDurasiMenit(){
		return durasiMenit;
	}
	public String getRating(){
		return rating;
	}
	public String getAsalFilm(){
		return asalFilm;
	}
	public void setJudul(String judul){
		this.judul = judul;
	}
	public void setGenre(String genre){
		this.genre = genre;
	}
	public void setDurasiMenit(int durasiMenit){
		this.durasiMenit = durasiMenit;
	}
	public void setRating(String rating){
		this.rating = rating;
	}
	public void setAsalFilm(String asalFilm){
		this.asalFilm = asalFilm;
	}
	
    public void printInfo(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("Judul       : " + this.getJudul());
        System.out.println("Genre       : " + this.getGenre());
        System.out.println("Durasi      : " + this.getDurasiMenit() + " menit");
        System.out.println("Rating      : " + this.getRating());
        System.out.println("Jenis       : Film " + this.getAsalFilm());
        System.out.println("------------------------------------------------------------------");
    }

}//komen
	