package theater;
import movie.Movie;
import ticket.Ticket;

import java.util.ArrayList;

public class Theater{
	private String namaBioskop;
	private int jumlahSaldo;
	private ArrayList<Ticket> listTicket;
	private Movie[] listMovie;
	public static int pendapatan;
	
	public Theater(String namaBioskop, int jumlahSaldo, ArrayList<Ticket> listTicket, Movie[] listMovie){
		this.namaBioskop = namaBioskop;
		this.jumlahSaldo = jumlahSaldo;
		this.listTicket = listTicket;
		this.listMovie = listMovie;
		pendapatan += jumlahSaldo;
	}
	
	public void setNamaBioskop(String namaBioskop){
		this.namaBioskop = namaBioskop;
	}
	public void setJumlahSaldo(int jumlahSaldo){
		this.jumlahSaldo += jumlahSaldo;
	}
	public void setListTicket(ArrayList<Ticket> listTicket){
		this.listTicket = listTicket;
	}
	public void setListMovie(Movie[] listMovie){
		this.listMovie = listMovie;
	}
	public String getNamaBioskop(){
		return namaBioskop;
	}
	public int getJumlahSaldo(){
		return jumlahSaldo;
	}
	public ArrayList<Ticket> getListTicket(){
		return listTicket;
	}
	public Movie[] getListMovie(){
		return listMovie;
	}
	
	public void printInfo(){
		System.out.println("------------------------------------------------------------------");
		System.out.println("Bioskop\t\t\t: " + this.namaBioskop);
		System.out.println("Saldo Kas\t\t: " + this.jumlahSaldo);
		System.out.println("Jumlah tiket tersedia\t: " + this.listTicket.size());
		System.out.print("Daftar Film Tersedia\t: ");
		for (int a=0;a<this.getListMovie().length;a++){
            System.out.print(this.getListMovie()[a].getJudul());
            if (a!=this.getListMovie().length-1){
                System.out.print(", ");
            }else{
                System.out.print("\n");
            }
        }
		System.out.println("------------------------------------------------------------------");
	}
	
	public static void printTotalRevenueEarned(Theater[] listTheater){
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + Theater.pendapatan);
		System.out.println("------------------------------------------------------------------");
        for (int a=0;a<listTheater.length;a++){
            System.out.println("Bioskop\t\t: " + listTheater[a].getNamaBioskop());
            System.out.println("Saldo Kas\t: Rp. " + listTheater[a].getJumlahSaldo() + "\n\n");
        }
        System.out.println("------------------------------------------------------------------");		
		
	}
}//komen