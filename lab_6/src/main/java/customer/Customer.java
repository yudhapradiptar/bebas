package customer;
import theater.Theater;
import ticket.Ticket;
import movie.Movie;
public class Customer{
	private String nama;
	private boolean jenisKelamin;
	private int umur;
	
    public Customer(String nama, boolean jenisKelamin, int umur) {
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
    }
	
	public String getNama(){
		return nama;
	}
	public boolean getJenisKelamin(){
		return jenisKelamin;
	}
	public int getUmur(){
		return umur;
	}
	public void setNama(String nama){
		this.nama=nama;
	}
	public void setJenisKelamin(boolean jenisKelamin){
		this.jenisKelamin = jenisKelamin;
	}
	public void setUmur(int umur){
		this.umur=umur;
	}
	
	public Ticket orderTicket(Theater namaBioskop,String judulFilm,String jadwal_tayang,String biasa3D){
		Ticket karcis = null;
		boolean biasa3DBoolean = true;
		if(biasa3D.equals("Biasa")){
			biasa3DBoolean = false;
		}
        for (Ticket tiket:namaBioskop.getListTicket()) {
            if (tiket.getFilm().getJudul().equals(judulFilm) && tiket.getJadwal().equals(jadwal_tayang) && tiket.getBiasa3D() ==biasa3DBoolean) {
                karcis = tiket;
                namaBioskop.getListTicket().remove(tiket);
                break;
            }
        }
        if (karcis !=null){
            if (karcis.getFilm().getRating().equals("Umum")) {
                System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis " + biasa3D + " di " + namaBioskop.getNamaBioskop() + " pada hari " + karcis.getJadwal() + " seharga Rp. "+ karcis.totalHarga());
                namaBioskop.setJumlahSaldo(karcis.totalHarga());
                Theater.pendapatan+=karcis.totalHarga();
            } else if (karcis.getFilm().getRating().equals("Remaja")) {
                if (this.getUmur() >= 13) {
                    System.out.println(this.nama + " telah membeli tiket " + judulFilm+ " jenis " + biasa3D + " di " + namaBioskop.getNamaBioskop() + " pada hari " + karcis.getJadwal() + " seharga Rp. "+ karcis.totalHarga());
                    namaBioskop.setJumlahSaldo(karcis.totalHarga());
                    Theater.pendapatan+=karcis.totalHarga();
                } else {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " + karcis.getFilm().getJudul() + " dengan rating " + karcis.getFilm().getRating());
                }
            } else if (karcis.getFilm().getRating().equals("Dewasa")) {
                if (this.getUmur() >= 17) {
                    System.out.println(this.nama + " telah membeli tiket " + judulFilm+ " jenis " + biasa3D + " di " + namaBioskop.getNamaBioskop() + " pada hari " + karcis.getJadwal() + " seharga Rp. "+ karcis.totalHarga());
                    namaBioskop.setJumlahSaldo(karcis.totalHarga());
                    Theater.pendapatan+=karcis.totalHarga();
                } else{
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " + karcis.getFilm().getJudul() + " dengan rating " + karcis.getFilm().getRating());
                }
            }
        }else{
            System.out.println("Tiket untuk film " + judulFilm+ " jenis " + biasa3D + " dengan jadwal " + jadwal_tayang + " tidak tersedia di " + namaBioskop.getNamaBioskop());
        }
        return karcis;
    }

    public void findMovie(Theater namaBioskop, String judulFilm){
        boolean isExist = false;
        Movie film = null;
        for(int a=0;a<namaBioskop.getListMovie().length;a++){
            if (namaBioskop.getListMovie()[a].getJudul().equals(judulFilm)) {
                isExist = true;
                film = namaBioskop.getListMovie()[a];
                break;
            }
        }
        if (isExist){
			film.printInfo();

        }else{
            System.out.println("Film " + judulFilm + " yang dicari " + this.getNama() + " tidak ada di bioskop " + namaBioskop.getNamaBioskop());
        }
    }
		
	}
//komen