package ticket;
import movie.Movie;
import theater.Theater;
import customer.Customer;
public class Ticket{
	private Movie film;
	private String jadwal;
	private boolean biasa3D;
	private static final int harga = 60000;
	
	public Ticket(Movie film, String jadwal, boolean biasa3D){
		this.film = film;
		this.jadwal = jadwal;
		this.biasa3D = biasa3D;
	}
	
	public Movie getFilm(){
		return film;
	}
	public void setFilm(Movie film){
		this.film = film;
	}
	public String getJadwal(){
		return jadwal;
	}
	public void setJadwal(String jadwal){
		this.jadwal = jadwal;
	}
	public boolean getBiasa3D(){
		return biasa3D;
	}
	public void setBiasa3D(boolean biasa3D){
		this.biasa3D = biasa3D;
	}
	
	public int totalHarga(){
		int harga_baru = harga;
		if(this.jadwal.equals("Sabtu")||this.jadwal.equals("Minggu")){
			harga_baru+=40000;
		}
		if(this.biasa3D == true){
			harga_baru+=(harga_baru/5);
		}
		return harga_baru;
		
	}
	
    public void printInfo(){
        String strjenis_tiket = "";
        if (this.getBiasa3D()==true){
            strjenis_tiket = "3 Dimensi";
        }else{
            strjenis_tiket = "Biasa";
        }
        System.out.println("------------------------------------------------------------------");
        System.out.println("Film            : " + this.getFilm());
        System.out.println("Jadwal Tayang   : " + this.getJadwal());
        System.out.println("Jenis           : " + strjenis_tiket);
        System.out.println("------------------------------------------------------------------");
    }	
}//komen
	